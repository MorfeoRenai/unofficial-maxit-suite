.PHONY: binary env_var install

name=maxit-bin
wdir=$(shell pwd)
bindir=~/.local/bin
sharedir=~/.local/share
RCSBROOT=$(sharedir)/$(name)

binary:
	cd etc/; bash binary.sh


env_var:
	echo  >> ~/.bashrc
	echo  >> ~/.bashrc
	echo '# >>> Maxit env variable >>>' >> ~/.bashrc
	echo 'export RCSBROOT=$(RCSBROOT)' >> ~/.bashrc
	echo '# <<< Maxit env variable <<<' >> ~/.bashrc


install: env_var binary
	mkdir $(RCSBROOT)
	cp -r bin $(RCSBROOT)/bin
	cp -r data $(RCSBROOT)/data
	cp -r etc $(RCSBROOT)/etc
	cp -r procheck $(RCSBROOT)/procheck
	cp LICENSE $(RCSBROOT)/LICENSE
	cp README.md $(RCSBROOT)/README.md
	ln $(RCSBROOT)/bin/maxit* $(bindir)/$(name)
