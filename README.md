# MAXIT Program Suite v8.120 (Feb 2008) unofficial installer

I wrote a Makefile for an quick-and-dirty installation for an old version of MAXIT.
I took the binary release from the [official site](https://sw-tools.rcsb.org/apps/MAXIT/binary.html).

The MAXIT Program Suite was developed by the PDB (Protein Data Bank) and
NDB (Nucleic Acid Database) to assist in the processing and curation of
macromolecular structure data.

## Features

(1) Reading and writing PDB and mmCIF format files, and translating between
file formats.

(2) Performing a variety of consistency checks on coordinates, sequence, and
crystal data.

(3) Constructing, transforming, and merging information between formats in
order to minimize the human intervention during file translation and data
processing operations.

(4) Residue numbering in the coordinate section is aligned with the sequence

(5) Atoms in standard and nonstandard residues and atoms in ligands are
reordered and renamed in a standard manner. The residue and ligand description
is held in mmCIF dictionary.

(6) Detection of missing or additional atoms.

(7) Submodule "procheck", which does additional structure checking.

## Installation

This program is for **Linux only**, and it requires `git` and `make`.

You can edit the `Makefile` and choose your intallation directories and other variables.

``` bash
git clone https://gitlab.com/MorfeoRenai/unofficial-maxit-suite.git
cd unofficial-maxit-suite/
make install
```
